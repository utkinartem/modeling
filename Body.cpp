#include <iostream>
#include "Body.h"

Body::Body()
    :m(0.),
     r(Vector2D()),
     v(Vector2D())
{

};

Body::Body(double m_)
    :r(Vector2D()),
     v(Vector2D())
{
    if (m_ > 0)
        m = m_;
    else
        throw std::string("mass is invalid");
};

Body::Body(double m_, Vector2D r_, Vector2D v_)
    :r(r_),
     v(v_)
{
    if (m_ > 0)
        m = m_;
    else
        throw std::string("mass is invalid");
};

double Body::getM() const
{
    return (m);
};
