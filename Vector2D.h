#pragma once

class Vector2D
{
    public:
    double x;   //m
    double y;   //m

    Vector2D();
    Vector2D(double x, double y);

    Vector2D operator=(const Vector2D& v);
    Vector2D operator+=(const Vector2D& v);
    Vector2D operator+(const Vector2D& v);
    Vector2D operator-(const Vector2D& v);
    Vector2D operator*(const double& num);
    Vector2D operator/(const double& num);

    double abs() const;
};

double scalarProduct(Vector2D v1, Vector2D v2);
