#include <iostream>
#include <cmath>
#include <fstream>
#include "Vector2D.h"
#include "Body.h"

using namespace std;

int main()
{
    try
    {
        int n = 2;

        Body * body_array = new Body [n];

        body_array[0] = Body(7.35e22, Vector2D(-384.4e6 + 149.8e9, 0), Vector2D(0, -1.023e3 + 29.783e3));
        body_array[1] = Body(5.96e24, Vector2D(149.8e9,0), Vector2D(0, 29.783e3));

        const double G = 6.67e-11;
        const double dt = 0.01 * 24 * 3600;
        double t = 0;

        do
        {
            for (int i = 0; i < n; i++)
            {
                Vector2D F_i;
                Vector2D a_i;
                for (int j = 0; j < n; j++)
                {
                    if (i != j)
                    {
                        Vector2D dr_ij = body_array[i].r - body_array[j].r;
                        Vector2D e_ij = dr_ij / dr_ij.abs();
                        Vector2D F_ij = dr_ij * (-G * body_array[i].getM() * body_array[j].getM() / dr_ij.abs() / dr_ij.abs() / dr_ij.abs());
                        F_i += F_ij;
                    }
                }
                a_i = F_i / body_array[i].getM();
                body_array[i].v += a_i * dt;
            }
            for (int i = 0; i < n; i++)
                body_array[i].r += body_array[i].v * dt;
            t += dt;
        } while (body_array[0].r.y - body_array[1].r.y <= 0);

        cout << t * 2. / 24. / 3600. << endl;
    }

    catch(string& error)
    {
        cout << error;
    }
}
