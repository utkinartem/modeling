#pragma once
#include "Vector2D.h"

class Body
{
    double m;
public:
    Vector2D r;
    Vector2D v;

    Body();
    Body(double m_);
    Body(double m_, Vector2D r_, Vector2D v_);

    double getM() const;
};
