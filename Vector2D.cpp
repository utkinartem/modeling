#include "Vector2D.h"
#include <cmath>

Vector2D::Vector2D()
    :x(0.),
     y(0.)
{
}

Vector2D::Vector2D(double x_, double y_)
    :x(x_),
     y(y_)
{
}

Vector2D Vector2D::operator=(const Vector2D& v)
{
    x=v.x;
    y=v.y;
    return *this;
}

Vector2D Vector2D::operator+=(const Vector2D& v)
{
    x+=v.x;
    y+=v.y;
    return *this;
}

Vector2D Vector2D::operator+(const Vector2D& v)
{
    Vector2D v_;
    v_.x=x+v.x;
    v_.y=y+v.y;
    return v_;
}

Vector2D Vector2D::operator-(const Vector2D& v)
{
    Vector2D v_;
    v_.x=x-v.x;
    v_.y=y-v.y;
    return v_;
}

Vector2D Vector2D::operator*(const double& num)
{
    Vector2D v;
    v.x=x*num;
    v.y=y*num;
    return v;
}

Vector2D Vector2D::operator/(const double& num)
{
    Vector2D v;
    v.x=x/num;
    v.y=y/num;
    return v;
}

double Vector2D::abs() const
{
    return sqrt(x*x+y*y);
}

double scalarProduct(Vector2D v1, Vector2D v2)
{
    return v1.x * v2.y + v1.y * v2.x;
}
